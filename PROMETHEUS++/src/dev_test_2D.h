// COPYRIGHT 2015-2019 LEOPOLDO CARBAJAL

/*	This file is part of PROMETHEUS++.

    PROMETHEUS++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    any later version.

    PROMETHEUS++ is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with PROMETHEUS++.  If not, see <https://www.gnu.org/licenses/>.
*/

/* 	ACKNOWLEDGEMENTS:
	+ Francisco Calderon
*/

#ifndef DEV_TEST_2D_H
#define DEV_TEST_2D_H

#include <iostream>
#include <vector>
#include <armadillo>
#include <ctime>

#include "types.h"

#include <omp.h>
#include "mpi_main.h"

using namespace std;
using namespace arma;

class DEV_TEST_2D{

public:

	DEV_TEST_2D();

};

#endif
