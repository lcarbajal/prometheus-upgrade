// COPYRIGHT 2015-2019 LEOPOLDO CARBAJAL

/*	This file is part of PROMETHEUS++.

    PROMETHEUS++ is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    any later version.

    PROMETHEUS++ is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with PROMETHEUS++.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "randomStart.h"

template <class IT> RANDOMSTART<IT>::RANDOMSTART(const simulationParameters * params){
	// Unitary vector along B field
	b1 = {params->BGP.Bx, params->BGP.By, params->BGP.Bz};
	b1 = arma::normalise(b1);

	if (arma::dot(b1,y) < PRO_ZERO){
		b2 = arma::cross(b1,y);
	}else{
		b2 = arma::cross(b1,z);
	}

	// Unitary vector perpendicular to b1 and b2
	b3 = arma::cross(b1,b2);
}


template <class IT> void RANDOMSTART<IT>::ringLikeVelocityDistribution(const simulationParameters * params, IT * ions){
	arma_rng::set_seed_random();
	ions->X = randu<mat>(ions->NSP,3);

	ions->V = zeros(ions->NSP,3);
	ions->Ppar = zeros(ions->NSP);
	ions->g = zeros(ions->NSP);
	ions->mu = zeros(ions->NSP);

	// We scale the positions
	ions->X.col(0) *= params->mesh.LX;
	ions->X.col(1) *= params->mesh.LY;

	arma::vec R = randu(ions->NSP);
	arma_rng::set_seed_random();
	arma::vec phi = 2.0*M_PI*randu<vec>(ions->NSP);

	arma::vec V2 = ions->VTper*cos(phi);
	arma::vec V3 = ions->VTper*sin(phi);

	arma_rng::set_seed_random();
	phi = 2.0*M_PI*randu<vec>(ions->NSP);

	arma::vec V1 = ions->VTpar*sqrt( -log(1.0 - R) ) % sin(phi);

	for(int pp=0;pp<ions->NSP;pp++){
		ions->V(pp,0) = V1(pp)*dot(b1,x) + V2(pp)*dot(b2,x) + V3(pp)*dot(b3,x);
		ions->V(pp,1) = V1(pp)*dot(b1,y) + V2(pp)*dot(b2,y) + V3(pp)*dot(b3,y);
		ions->V(pp,2) = V1(pp)*dot(b1,z) + V2(pp)*dot(b2,z) + V3(pp)*dot(b3,z);

		ions->g(pp) = 1.0/sqrt( 1.0 - dot(ions->V.row(pp),ions->V.row(pp))/(F_C*F_C) );
		ions->mu(pp) = 0.5*ions->g(pp)*ions->g(pp)*ions->M*( V2(pp)*V2(pp) + V3(pp)*V3(pp) )/params->BGP.Bo;
		ions->Ppar(pp) = ions->g(pp)*ions->M*V1(pp);
	}

	ions->avg_mu = mean(ions->mu);
}


//This function creates a Maxwellian velocity distribution for ions with a homogeneous spatial distribution.
template <class IT> void RANDOMSTART<IT>::maxwellianVelocityDistribution(const simulationParameters * params, IT * ions){
	arma_rng::set_seed_random();
	ions->X = randu<mat>(ions->NSP,3);

	ions->V = zeros(ions->NSP,3);
	ions->Ppar = zeros(ions->NSP);
	ions->g = zeros(ions->NSP);
	ions->mu = zeros(ions->NSP);

	// We scale the positions
	ions->X.col(0) *= params->mesh.LX;
	ions->X.col(1) *= params->mesh.LY;

	arma::vec R = randu(ions->NSP);
	arma_rng::set_seed_random();
	arma::vec phi = 2.0*M_PI*randu<vec>(ions->NSP);

	arma::vec V2 = ions->VTper*sqrt( -log(1.0 - R) ) % cos(phi);
	arma::vec V3 = ions->VTper*sqrt( -log(1.0 - R) ) % sin(phi);

	arma_rng::set_seed_random();
	R = randu<vec>(ions->NSP);
	arma_rng::set_seed_random();
	phi = 2.0*M_PI*randu<vec>(ions->NSP);

	arma::vec V1 = ions->VTpar*sqrt( -log(1.0 - R) ) % sin(phi);

	for(int pp=0;pp<ions->NSP;pp++){
		ions->V(pp,0) = V1(pp)*dot(b1,x) + V2(pp)*dot(b2,x) + V3(pp)*dot(b3,x);
		ions->V(pp,1) = V1(pp)*dot(b1,y) + V2(pp)*dot(b2,y) + V3(pp)*dot(b3,y);
		ions->V(pp,2) = V1(pp)*dot(b1,z) + V2(pp)*dot(b2,z) + V3(pp)*dot(b3,z);

		ions->g(pp) = 1.0/sqrt( 1.0 - dot(ions->V.row(pp),ions->V.row(pp))/(F_C*F_C) );
		ions->mu(pp) = 0.5*ions->g(pp)*ions->g(pp)*ions->M*( V2(pp)*V2(pp) + V3(pp)*V3(pp) )/params->BGP.Bo;
		ions->Ppar(pp) = ions->g(pp)*ions->M*V1(pp);
	}

	ions->avg_mu = mean(ions->mu);
}

template class RANDOMSTART<oneDimensional::ionSpecies>;
template class RANDOMSTART<twoDimensional::ionSpecies>;
